#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 16 11:08:37 2018

@author: anish
"""

import socket, sys
import struct

def checksum(msg) :
    s = 0
    for i in range(0, len(msg), 2) :
        # 16 bit addition. 2 bytes at a time.
        w = ord(msg[i]) + (ord(msg[i + 1]) << 8)
        s = s + w
        
    s = (s>>16) + (s & 0xffff);
    s = s + (s >> 16);
    
    s = ~s & 0xffff
    return s

def ipHeader(source_ip, dest_ip, prot_num = 17) :
    #version
    ip_ver = 4
    # ip header length in units of 32bits.
    ip_ihl = 5
    # type of service. precedence. normal/low delay, etc.
    ip_tos = 0
    # total length in of octets. set to 0. kernel will fill correct length.
    ip_totlen = 0
    # identification number. used while defragmentation.
    ip_identi = 55445
    #bit0 reserved 0. bit1 0 for 'may fragment'. bit2 last fragment(0) or not
    ip_frag_off = 0
    #time to live. number of hops
    ip_ttl = 255
    # protocol. indicates next level protocol used in data portion of datagram.
    ip_prot = prot_num
    # checksum. kernel will set.
    ip_check = 0
    # source address
    ip_source = socket.inet_aton(source_ip)
    #dest address
    ip_dest = socket.inet_aton(dest_ip)
    
    #join ip_ver(4bit) and ip_ihl(4bit) to 1 byte.
    ip_ver_ihl = (ip_ver << 4) + ip_ihl
    
    # > bigendian. B int 1Byte.H int 2byte.s string.
    ip_hdr = struct.pack('>BBHHHBBH4s4s', ip_ver_ihl, ip_tos,ip_totlen, ip_identi, ip_frag_off, ip_ttl, ip_prot, ip_check, ip_source, ip_dest)
    return ip_hdr

def udpPacket(source_port, dest_port, data = 'pong\n') :
    udp_src_port = source_port
    udp_dest_port = dest_port
    udp_data = data
    udp_length = 8 + len(udp_data)
    udp_checksum = 0

    udp_hdr = struct.pack(">HHHH", udp_src_port, udp_dest_port, udp_length, udp_checksum)
    """
    check sum requires the source ip. currently we are setting it as 0.0.0.0.
    This automatically selects the interface and resolves to either 127.0.0.1
    (when in loopback) or other ips available. For checksum, we need to put
    this ip in the pseudo header.
    To know this i ll have to change a lot of code ;)
    There are certain workarounds
    """
#    udp_checksum = checksum(udp_hdr + udp_data)
#    udp_hdr = struct.pack(">HHH", udp_src_port, udp_dest_port, udp_length) + struct.pack("H", udp_checksum)
#    udp_checksum = 0
    return (udp_hdr)


if len(sys.argv) > 2 :
    print "Wrong usage! correct usage : sudo python udpserver.py [<port_num>]"
    sys.exit()
elif len(sys.argv) == 2 :
    server_port = sys.argv[1]
else :
    server_port = '1234'

server_ip = '0.0.0.0'
    
# make socket
try :
    sout = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_RAW)
    sout.bind((server_ip, int(server_port)))
except socket.error, msg :
    print 'Socket could not be created. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
    sys.exit()

# make socket
try :
    sin = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sin.bind((server_ip, int(server_port)))
except socket.error, msg :
    print 'Socket could not be created. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
    sys.exit()

while 1 :    
    pac, addr = sin.recvfrom(1024)
    ip_hdr = ipHeader(server_ip, addr[0])
    udp_packet = udpPacket(int(server_port), addr[1])
    packet = ip_hdr + udp_packet + 'pong\n'
    try :
        print "sending pong to ", addr[0], ":", addr[1]
        sout.sendto(packet, (addr[0], addr[1]))
    except socket.error, msg : 
        print (msg)
    








