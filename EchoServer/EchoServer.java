import java.io.*;
import java.net.*;

public class EchoServer {
	public static void main(String args[]) {
		try {
			// create a socket and bind to port
			ServerSocket ss = new ServerSocket(5555);

			System.out.println("Server running at " + 
				ss.getInetAddress() + " port " +
				ss.getLocalPort());

			//handle client connections
			while (true) {
				Socket cs = ss.accept();

				System.out.println("new client : " + 
					cs.getInetAddress() + " port " + 
					cs.getPort());
				
				//allocate a thread for this client
				ClientThread clientThread = new ClientThread(cs);
				clientThread.start();
			}

		} catch (Exception e) {
			System.err.println(e);
		}
	}
}

class ClientThread extends Thread {
	Socket cs;

	//constructor
	public ClientThread(Socket cs) {
		this.cs = cs;
	}

	public void run() {
		try {
			//get input from client
			BufferedReader buf = new BufferedReader(new InputStreamReader (this.cs.getInputStream()));
			String clientInput = null;
			while (true) {
				clientInput = buf.readLine();
				if (clientInput == null) {
					System.err.println("Connection lost to " + cs.getInetAddress() +
					 ":" + cs.getPort());
					break;
				}
				//if message not null, print it along with port number
				System.out.println(cs.getPort() + " : " + clientInput);

				//echo it back to client
				DataOutputStream dout = new DataOutputStream(this.cs.getOutputStream());
				dout.writeChars(clientInput + "\n");
				dout.flush();
			}
			// DataInputStream dis = new DataInputStream(this.cs.getInputStream());
			// String clientInput = (String)dis.readUTF();

			
			this.cs.close();
			this.stop();
		} catch (IOException e) {
			System.err.println("io error : " + e);
		}
	}
}