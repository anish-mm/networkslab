#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 15 21:13:06 2018

@author: anish
"""
import socket, sys
import struct
import time

def checksum(msg) :
    s = 0
    for i in range(0, len(msg), 2) :
        # 16 bit addition. 2 bytes at a time.
        w = ord(msg[i]) + (ord(msg[i + 1]) << 8)
        s = s + w
        
    s = (s>>16) + (s & 0xffff);
    s = s + (s >> 16);
    
    s = ~s & 0xffff
    return s

def ipHeader(source_ip, dest_ip, prot_num = 1) :
    #version
    ip_ver = 4
    # ip header length in units of 32bits.
    ip_ihl = 5
    # type of service. precedence. normal/low delay, etc.
    ip_tos = 0
    # total length in of octets. set to 0. kernel will fill correct length.
    ip_totlen = 0
    # identification number. used while defragmentation.
    ip_identi = 55445
    #bit0 reserved 0. bit1 0 for 'may fragment'. bit2 last fragment(0) or not
    ip_frag_off = 0
    #time to live. number of hops
    ip_ttl = 255
    # protocol. indicates next level protocol used in data portion of datagram.
    ip_prot = prot_num
    # checksum. kernel will set.
    ip_check = 0
    # source address
    ip_source = socket.inet_aton(source_ip)
    #dest address
    ip_dest = socket.inet_aton(dest_ip)
    
    #join ip_ver(4bit) and ip_ihl(4bit) to 1 byte.
    ip_ver_ihl = (ip_ver << 4) + ip_ihl
    
    # > bigendian. B int 1Byte.H int 2byte.s string.
    ip_hdr = struct.pack('>BBHHHBBH4s4s', ip_ver_ihl, ip_tos,ip_totlen, ip_identi, ip_frag_off, ip_ttl, ip_prot, ip_check, ip_source, ip_dest)
#    print ("\n", ip_hdr, '\n')
    return ip_hdr


def icmpEchoReq(seq_num) :
    # type = 8 for echo request
    icmp_type = 8
    # code
    icmp_code = 0
    #checksum. now set to 0.
    icmp_checksum = 0
    # identifier : the identifier might be used like a port in TCP or UDP to
    # identify a session
    icmp_identifier = 12345
    # sequence number :might be incremented on each echo request sent.
    icmp_seq_num = seq_num

    data = 'ping'
    
    icmp_hdr =  struct.pack('>BBHHH', icmp_type, icmp_code,icmp_checksum,
                            icmp_identifier, icmp_seq_num)
    icmp_msg = icmp_hdr + data
    icmp_checksum = checksum(icmp_msg)
    
    # remake the packet.
    icmp_hdr = struct.pack('>BB', icmp_type, icmp_code) + struct.pack('H',
                          icmp_checksum) + struct.pack('>HH',
                                       icmp_identifier, icmp_seq_num)
    
    icmp_msg = icmp_hdr + data
    return icmp_msg, icmp_identifier


# create a raw socket
try :
    s = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_RAW)
except socket.error, msg :
    print 'Socket could not be created. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
    sys.exit()
# socket for receiving reply 
try :
    sin = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_ICMP)
except socket.error, msg :
    print 'Socket could not be created. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
    sys.exit()

# set timeout time
sin.settimeout(2)

if len(sys.argv) == 1 :
    print 'wrong usage! correct usage : python ping.py <host name>'
    sys.exit()

source_ip = socket.gethostbyname(socket.gethostname())
dest_ip = socket.gethostbyname(sys.argv[1])
source_ip = '0.0.0.0'

print "PING", dest_ip
seq_num = 0
while(1) :
    ip_hdr = ipHeader(source_ip, dest_ip)
    icmp_msg, icmp_identifier = icmpEchoReq(seq_num)
    
    packet = ip_hdr + icmp_msg
    
    try :
        # start time
        start = time.time()
        s.sendto(packet, (dest_ip, 0))
    except socket.error, msg : 
        print (msg)
    
    try :
        packe, (dst, _) = sin.recvfrom(1024)
        end = time.time()
    except socket.timeout :
        print "icmp_seq=",seq_num," timed out"
        seq_num += 1
        continue

    ver_ihl, = struct.unpack('B', packe[0])
    ihl = ver_ihl & 0xf
    print "ihl = ", ihl
    icmp_packe = packe[ihl * 4:]
    _, _, _, rec_identi, rec_seq = struct.unpack('>BBHHH',icmp_packe[0:8])
    
    if rec_identi == icmp_identifier and rec_seq == seq_num :
        ttl = struct.unpack('B', packe[8])
        time_taken = (end - start) * 1000
        size = len(packe)
        seq_num += 1
        print size, "bytes from ", dst, ": icmp_seq=", rec_seq, " ttl=", ttl[0], "time=", round(time_taken), " ms"
    else :
        print rec_identi[0], icmp_identifier
        print rec_seq[0], seq_num
        sys.exit()
        
    
   
    
    
    
    
    
    
    
    
    
    