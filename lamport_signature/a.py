#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  6 07:05:36 2018

@author: anish
"""
import random
import hashlib

def privKeyGen():
    rand_num_list = [random.getrandbits(512) for i in xrange(0, 2*512)]
    priv_key_list = ["{0:x}".format(rand_num).zfill(128) for rand_num in rand_num_list]
    priv_key = reduce(lambda x, y:x + y, priv_key_list)
    f = open("lamport.key", "w")
    f.write(priv_key)
    f.close()
    print "private key generated"
    return priv_key

def pubKeyGen(x):
    priv_key_list = [x[i * 128 : (i + 1) * 128] for i in range(2 * 512)]
    pub_key_list = [hashlib.sha512(y).hexdigest() for y in priv_key_list]
    pub_key = reduce(lambda x, y : x + y, pub_key_list)
    f = open("lamport.pub", "w")
    f.write(pub_key)
    f.close()
    print "public key gererated"
    return pub_key

def signMessage(msgfile, priv_key):
    f = open(msgfile)
    msg = f.read()
    msg_hash = hashlib.sha512(msg).hexdigest()
    bin_hash = "{0:b}".format(int(msg_hash, 16)).zfill(512)
    index_list = [256*i + 128*int(bin_hash[i]) for i in range(512)]
    sign_list = [priv_key[i:i+128] for i in index_list]
    sign = reduce(lambda x, y : x + y, sign_list)
    f = open("sign.sig", "w")
    f.write(sign)
    f.close()
    return sign

key = privKeyGen()
pubKeyGen(key)
signMessage("msg.txt", key)
