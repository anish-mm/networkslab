#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  6 13:57:31 2018

@author: anish
"""

import hashlib

def get_content(file_name) :
    f = open(file_name);
    content = f.read()
    return content

def verify_sig(sig, msg, pub) :
    msg_hash = hashlib.sha512(msg).hexdigest()
    bin_hash = "{0:b}".format(int(msg_hash, 16)).zfill(512)
    index_list = [256*i + 128*int(bin_hash[i]) for i in range(512)]
    sig_hash_list = [hashlib.sha512(sig[i*128:(i+1)*128]).hexdigest() for i in range(512)]
    pub_sel_list = [pub[i:i+128] for i in index_list]

    flag = 0
    for i in range(512) :
        if sig_hash_list[i] != pub_sel_list[i]:
            flag = 1
            break
    if flag == 0:
        print "Signature is correct. Verified."
    else:
        print "Wrong Signature!!"
        
pub = get_content("lamport.pub")
sig = get_content("sign.sig")
msg = get_content("msg.txt")
verify_sig(sig, msg, pub)
