import java.io.*;
import java.net.*;

public class NetCat {
	public static void main(String args[]) throws Exception {
		// get host name and port number from command line args.
		String hostName = args[0];
		int portNumber = Integer.parseInt(args[1]);

		try {
			Socket cs = new Socket(hostName, portNumber);
			DataOutputStream dout = new DataOutputStream(cs.getOutputStream());
			BufferedReader bufReader = new BufferedReader(new InputStreamReader(cs.getInputStream()));

			//start thread to listen to messages from the server.
			ListenerThread listener = new ListenerThread(bufReader);
			listener.start();

			//get messages from standard input and send to server.
			String userInput = null;
			while (true) {
				userInput = System.console().readLine();
				if (userInput != null) {
					dout.writeBytes(userInput + "\r\n");
				}
			}
		} catch (Exception e) {
			System.err.println(e);
			System.out.println("Connection closed.");
		}
	}
}

/*one thread to listen to messages from the server.*/
class ListenerThread extends Thread {
	private BufferedReader buf;

	public ListenerThread(BufferedReader bufReader) {
		this.buf = bufReader;
	}

	public void run() {
		int i;
		try {
			/* keep reading characters from the buffer.
			   Print if not null */
			while (true) {
				i = buf.read();
				if (i != -1) {
					System.out.print((char)i);
				}
			}
		} catch (Exception e) {
			System.err.println(e);
			System.out.println("\n Connection closed.");
		}
	}
}