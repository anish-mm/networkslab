#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 21 22:25:51 2018

@author: anish
"""

#hamming code [n = 2^r - 1, k = 2^r -r -1, d = 3]_2
#input x_{1, k}.
#generator matrix G_{n, k}
#encoding : y_{n, 1} = Gx
#parity check matrix H_{n, n-k} : tr(G).H = 0
#error detection : if tr(y).H != 0 error
#error correction : 1 <= decimal value of (tr(y).H) <= n, flip n th bit.
#decoding : x' = y[:k]

import numpy as np
import sys
flip = lambda x : '1' if x == '0' else '0'

def generate(n, k):
    I = np.identity(n-k, int)
    P = [map(int, "{0:b}".format(i).zfill(n-k)) for i in range(1, n + 1)]
    P = filter(lambda x : reduce(lambda a, b:a+b, x) != 1, P)
    H = np.vstack((P, I))
    print "H generated. H = "
    print H
    
    I = np.identity(k, int)
    Pt = np.vstack((P)).transpose()
    G = np.vstack((I, Pt))
    print "G generated. G = "
    print G    
    
    return G, H

def getHammingParams(r) :
    n = 2**r - 1
    k = n - r

    print "n calculated. n = ", n
    print "k calculated. k = ", k
    return n, k

# read valid binary string
def readBinaryString() :
    while 1 :
        ip = raw_input("Enter a valid binary string  ")
        if len(ip) == 0 :
            print "Empty string not allowed."
            continue
        red = reduce(lambda x, y:x and y, map(lambda x : int(x) == 1 or int(x) == 0, ip))
        if red == True :
            break
        else :
            print "Invalid. Not a binary string"
    return ip

def getr() :
    while 1 :
        r = input("Enter value of r :  ")
        if r <= 2 :
            print "invalid r"
        else :
            print "r is valid"
            break
    return r

def encode(ip, G, H, r, n, k) :
    l = len(ip)
    num_of_chunks = (l - 1) / k + 1
    print "Length of string = ", l
    print "Number of chunks = ", num_of_chunks

    i = 0
    C = str()
    while i < num_of_chunks :
        chunk = ip[i*k:(i+1)*k]
        print "\nchunk ", i + 1, " : ", chunk

        chunk_len = len(chunk)
        if chunk_len < k :
            print "length of chunk < ", k, "."
            print "padding with zero bits."
            zeroes = reduce(lambda x, y:x + y,['0' for w in range(1, k - chunk_len + 1)])
            chunk = chunk + zeroes
            print "padded string : ", chunk

        x = np.vstack(map(int, chunk))
        c = [ w[0] % 2 for w in np.dot(G, x)]
        print "encoded string : ", reduce(lambda x, y:str(x)+str(y), c)

        C = C + reduce(lambda x, y:str(x)+str(y), c)
        i += 1
    print "\nEncoding of whole string (possibly with paddings) : ", C
    return C

def decode(C, G, H, r, n, k, pad=0) :
    l = len(C)
    if l % n != 0 :
        print "Invalid string. Not properly encoded."
        sys.exit(0)
    print "decoding ", C
    num_of_chunks = (l - 1) / n + 1
    print "number of ", n, " length chunks = ", num_of_chunks
    i = 0
    x = str()
    while i < num_of_chunks :
        chunk = C[i*n:(i+1)*n]
        print "\nchunk ", i + 1, " : ", chunk
        i += 1
        y = np.vstack(map(int, chunk))
        synd = [w % 2 for w in np.dot(y.transpose(), H)[0]]
        print "syndrome = ", synd
        if 1 in synd :
            print "error detected. ";
            flag = 0
            for row in range(0, n) :
                if reduce(lambda x, y:x and y,H[row] == synd) :
                    print "matches row at index ", row
                    # flip corresponding bit in C
                    print "flipping bit at index ", row
                    chunk = chunk[:row]+flip(chunk[row])+chunk[row+1:]
                                                                          
                    flag = 1
                    break
            if flag == 0 :
                print "Couldn't correct error"
                print "decoded string (with errors) : ", reduce(lambda x, y:x+y, chunk[:k])
                continue
        else :
            print "no error detected. "
        print "decoded string : ", reduce(lambda x, y:x+y, chunk[:k])
        x = x + reduce(lambda x, y:x+y, chunk[:k])
    if pad > 0 :
        x = x[:-pad]
    print "\nWhole decoded string : ", x

while 1 :
    print "select "
    print "1. encode"
    print "2. decode"
    print "3. encode & decode"
    print "4. exit"
    choice = input("Enter choice (1-4) ")
    if choice == 4:
        break
    else:
        r = getr()
        n, k = getHammingParams(r)
        G, H = generate(n, k)
        ip = readBinaryString()
    if choice == 1 :
        encode(ip, G, H, r, n, k)
    elif choice == 2 :
        decode(ip, G, H, r, n, k)
    elif choice == 3:
        c = encode(ip, G, H, r, n, k)
        decode(c, G, H, r, n, k)
    else :
        print "invalid choice."

