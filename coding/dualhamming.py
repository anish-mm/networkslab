#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 22 20:44:39 2018

@author: anish
"""

import numpy as np
import sys
flip = lambda x : '1' if x == '0' else '0'
check = lambda y,x : filter(lambda (a,b):a==x, y)

def generateGandH(n, k) :  
    I = np.identity(k, int)
    P = [map(int, "{0:b}".format(i).zfill(k)) for i in range(1, n + 1)]
    P = filter(lambda x : reduce(lambda a, b:a+b, x) != 1, P)
    G = np.vstack((I, P))
    print "G generated. G = "
    print G
    
    I = np.identity(n-k, int)
    Pt = np.vstack((P)).transpose()
    H = np.vstack((Pt, I))    
    print "H generated. H = "
    print H    

    return G, H

def getDualHammingParams(r) :
    n = 2**r - 1
    k = r
    
    print "n calculated. n = ", n
    print "k calculated. k = ", k
    return n, k

def readBinaryString() :
    while 1 :
        ip = raw_input("Enter a valid binary string  ")
        if len(ip) == 0 :
            print "Empty string not allowed."
            continue
        red = reduce(lambda x, y:x and y, map(lambda x : int(x) == 1 or int(x) == 0, ip))
        if red == True :
            break
        else :
            print "Invalid. Not a binary string"
    return ip

def getr() :
    while 1 :
        r = input("Enter value of r :  ")
        if r <= 2 :
            print "invalid r"
        else :
            print "r is valid"
            break
    return r

def encode(ip, G, H, r, n, k) :
    l = len(ip)
    num_of_chunks = (l - 1) / k + 1
    print "Length of string = ", l
    print "Number of chunks = ", num_of_chunks
    
    i = 0
    C = str()
    while i < num_of_chunks :
        chunk = ip[i*k:(i+1)*k]
        print "\nchunk ", i + 1, " : ", chunk
        
        chunk_len = len(chunk)
        if chunk_len < k :
            print "length of chunk < ", k, "."
            print "padding with zero bits."
            zeroes = reduce(lambda x, y:x + y,['0' for w in range(1, k - chunk_len + 1)])
            chunk = chunk + zeroes
            print "padded string : ", chunk
        
        x = np.vstack(map(int, chunk))
        c = [ w[0] % 2 for w in np.dot(G, x)]
    
        print "encoded string : ", reduce(lambda x, y:str(x)+str(y), c)
        
        C = C + reduce(lambda x, y:str(x)+str(y), c)
        i += 1
    print "\nEncoding of whole string (possibly with paddings) : ", C
    return C

#create look up table for coset decoding
def createLookup(C, G, H, r, n, k, pad=0) :
    S = [map(int, "{0:b}".format(i).zfill(n)) for i in range(1, 2**n)]
    M = [(np.dot(x, H), x) for x in S]
    M = map(lambda (x,y) : (map(lambda z : z%2, x), y), M)
    zero = map(int, ''.zfill(n-k))
    M = filter(lambda (x,y) : x != zero, M)
    syn = []
    for (x, y) in M :
        c = check(syn, x)
        if len(c) == 0:
            syn = [(x,y)] + syn
        elif sum(c[0][1]) > sum(y) :
            syn = [(x,y)] + filter(lambda (a,b) : a != x, syn)
    
    return syn

def decode(C, G, H, r, n, k, pad=0) :
    l = len(C)
    if l % n != 0 :
        print "Invalid string. Not properly encoded."
        sys.exit(0)
    print "decoding ", C
    num_of_chunks = (l - 1) / n + 1
    print "number of ", n, " length chunks = ", num_of_chunks
    syn = createLookup(C, G, H, r, n, k, pad=0)
    zero = map(int, ''.zfill(n-k))
    i=0
    st = str()
    while i < num_of_chunks :
        chunk = C[i*n:(i+1)*n]
        print "\nchunk ", i + 1, " : ", chunk
        i += 1
        y = np.hstack(map(int, chunk))
        synd = [w % 2 for w in np.dot(y, H)]
        print "syndrome = ", synd
        if synd != zero :
            print "error detected"
            e = check(syn, synd)
            if len(e) == 0 : 
                print "can't correct"
                print "decoded, but uncorrected string : ", chunk[:k]
                st = st + chunk[:k]
                continue
            else :
                y = map(lambda (a,b):(a+b)%2, zip(e[0][1], y))
                print "error corrected"
                print "decoded and corrected string : ", ''.join(map(str, y))
        else :
            print "no error detected."
            print "decoded string : ", ''.join(map(str, y))[:k]
        st = st + ''.join(map(str, y))[:k]
    print "whole decoded string", st
    return st

while 1 :
    print "select "
    print "1. encode"
    print "2. decode"
    print "3. encode & decode"
    print "4. exit"
    choice = input("Enter choice (1-4) ")
    if choice == 4:
        break
    else:
        r = getr()
        n, k = getDualHammingParams(r)
        G, H = generateGandH(n, k)
        ip = readBinaryString()
    if choice == 1 :
        encode(ip, G, H, r, n, k)
    elif choice == 2 :
        decode(ip, G, H, r, n, k)
    elif choice == 3:
        c = encode(ip, G, H, r, n, k)
        decode(c, G, H, r, n, k)
    else :
        print "invalid choice."
