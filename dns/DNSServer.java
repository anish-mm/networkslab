/*
	usage java DNSServer [<port_number>]

*/


import java.io.*;
import java.net.*;

public class DNSServer {
	private static int DNS_SERVER_PORT = 53;

	public static void main(String args[]) throws Exception {
		if (args.length == 1) {
			DNS_SERVER_PORT = Integer.parseInt(args[0]);
		}

		// create socket
		DatagramSocket sock = new DatagramSocket(DNS_SERVER_PORT);
		System.out.print("DNS Server running....\n");
		while (true) {
			byte buf[] = new byte[1024];
			DatagramPacket packet = new DatagramPacket(buf, buf.length);
			sock.receive(packet);		

			System.out.println("new client : " + 
					packet.getAddress() + " port " + 
					packet.getPort());

			//allocate a thread for this client
			ServerThread serverThread = new ServerThread(buf, packet, sock);
			serverThread.start();
		}
	}
}

class ServerThread extends Thread {
	private byte buf[];
	private DatagramPacket reqPacket;
	private DatagramSocket sock;

	public ServerThread(byte buf[], DatagramPacket reqPacket, DatagramSocket sock) {
		this.buf = buf;
		this.reqPacket = reqPacket;
		this.sock = sock;
	}

	public void run() {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(baos);

		DataInputStream din = new DataInputStream(new ByteArrayInputStream(buf));

		try {
			// construct response frame

			// copy identifier from request packet. 2 bytes.
			dos.writeShort(din.readShort());

			// write the flags. QR is set to 1 indicating Response.
			dos.writeShort(0x8400); /* binary equivalent of 1000 0100 0000 0000 */

			// QDCOUNT : Number of questions in the question section : = 1
			dos.writeShort(0x0001);

			// ANCOUNT : Number of answers following : = 1
			dos.writeShort(0x0001);

			// NSCOUNT : = 0
			dos.writeShort(0x0);

			// ARCOUNT : = 0
			dos.writeShort(0x0);

			// copy question to the response.
			din.skipBytes(10);
			int flag = 0;
			int count = 0;
			int recLen = 0;
			String name[] = new String[10];
			while ((recLen = din.readByte()) > 0) {
				count++;
				dos.writeByte(recLen);
				// System.out.println(recLen);
				byte record[] = new byte[recLen];
				for (int i = 0; i < recLen; i++) {
					record[i] = din.readByte();
					dos.writeByte(record[i]);
				}
				System.out.println("Record : " + new String(record, "UTF-8"));
				name[count - 1] = new String(record, "UTF-8");
			}
			if (count != 3 || !(name[0].equals("www") && name[1].equals("james") && name[2].equals("bond"))) {
				System.out.println(count + " " + name[0] + "." + name[1] + "." + name[2]);
				System.err.println("cannot resolve host");
				flag = 1;
			} 
			// copy the null label.
			dos.writeByte(0x0);

			// Question TYPE : A record : 0x0001
			dos.writeShort(0x0001);

			// Class : 0x0001 : internet
			dos.writeShort(0x0001);
			byte dnsFrame[];
			if (flag == 1) {
				dnsFrame = baos.toByteArray();
				dnsFrame[3] = (byte)0x83;
				dnsFrame[7] = (byte)0x0;
			} else {
				// name
				dos.writeShort(0xC00C);

				// Answer TYPE : A record : 0x0001
				dos.writeShort(0x0001);

				// Class : 0x0001 : internet
				dos.writeShort(0x0001);

				// TTL
				dos.writeShort(0x0);
				dos.writeShort(0xffff);

				// RDLENGTH : length of RDATA field : 4bytes
				dos.writeShort(0x0004);

				// RDATA : the ip address corresponding to the query.
				for (int i = 0; i < 4; i ++) {
					dos.writeByte(0x7);
				}

				dnsFrame = baos.toByteArray();
			}

			// get address of client.
			InetAddress clientIpAddress = reqPacket.getAddress();
			int clientPort = reqPacket.getPort();

			// Sending the request
			DatagramPacket dnsResponsePacket = new DatagramPacket(dnsFrame, dnsFrame.length,
			 clientIpAddress, clientPort);
			sock.send(dnsResponsePacket);
			System.out.println("Response sent to " + clientIpAddress.getHostAddress() +
			 " : " + Integer.toString(clientPort));

		} catch (Exception e) {
			System.err.println(e);
		}
		
	}
}