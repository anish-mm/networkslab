/*
	usage : java DNSClient <name> [<server_address> [<port>]]
*/

import java.io.*;
import java.net.*;

public class DNSClient {
	// DNS servers work on port 53 by convention
	private static int DNS_SERVER_PORT = 53; // default port for DNS
	private static String DNS_SERVER_ADDRESS = "8.8.8.8"; // google's DNS server.

	public static void main(String args[]) throws Exception {
		// usage : java DNSClient <domain_name> <server_address>
		if (args.length == 3) {
			DNS_SERVER_PORT = Integer.parseInt(args[2]);
			DNS_SERVER_ADDRESS = args[1];
		} else if (args.length == 2) {
			DNS_SERVER_ADDRESS = args[1];
		} else if (args.length != 1) {
			System.err.println("Usage error. Use as : java DNSClient <name> [<server_address> [<port>]]");
			System.exit(0);
		}

		String domain = args[0];
		
		InetAddress ipAddress = InetAddress.getByName(DNS_SERVER_ADDRESS);

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(baos);

		// make DNS request frame
		
		// 16 bit identifier : some random number
		dos.writeShort(0x3456);

		//16 bits for flags : only the RD(Recursion Desired) bit is set to 1. All other bits set to 0.
		dos.writeShort(0x0100);

		// Question Count : Number of questions in the Question section.
		dos.writeShort(0x0001);

		//Answer Record Count : Number of resource records in the answer section.
		dos.writeShort(0x0000);

		// Authority Record Count : Number of resource records in the authority section.
		dos.writeShort(0x0000);

		// Additional Record Count : Number of resource records in the additional section.
		dos.writeShort(0x0000);

		/*
			Question Name : contains the domain name that is the subject of 
			the query.
			Standard DNS Name Notation : The domain name is encoded as follows. 
			Each label is encoded one after the next in the name field. Before 
			each label, a single byte is used that holds a binary number 
			indicating the number of characters in the label. Then, the label's 
			characters are encoded, one per byte. The end of the name is indicated 
			by a null label, representing the root.
		*/
		String domainParts[] = domain.split("\\.");
		System.out.println("Domain has " + domainParts.length + " parts.");

		for (int i = 0; i < domainParts.length; i++) {
			System.out.println("Writing : " + domainParts[i]);
			byte domainBytes[] = domainParts[i].getBytes("UTF-8");
			dos.writeByte(domainBytes.length);
			dos.write(domainBytes);
		}

		// end with null label. length is zero. write that byte.
		dos.writeByte(0x00);

		// Type 0x01 (Host Request)
		dos.writeShort(0x0001);

		// Question Class : 1 for internet(IN)
		dos.writeShort(0x0001);

		byte dnsFrame[] = baos.toByteArray();

		System.out.println("Sending " + dnsFrame.length + " bytes.");
		for (int i = 0; i < dnsFrame.length; i++) {
			System.out.print("0x" + String.format("%x", dnsFrame[i]) + " " );
		}

		// Sending the request
		DatagramSocket sock = new DatagramSocket();
		DatagramPacket dnsRequestPacket = new DatagramPacket(dnsFrame, dnsFrame.length,
		 ipAddress, DNS_SERVER_PORT);
		sock.send(dnsRequestPacket);

		// get response from DNS server
		byte buf[] = new byte[1024];
		DatagramPacket packet = new DatagramPacket(buf, buf.length);
		sock.receive(packet);

		System.out.println("\nReceived " + packet.getLength() + " bytes.");
		for (int i = 0; i < packet.getLength(); i++) {
			System.out.print("0x" + String.format("%x", buf[i]) + " ");
		}
		System.out.println("\n");

		DataInputStream din = new DataInputStream(new ByteArrayInputStream(buf));
		System.out.println("Transaction ID: 0x" + String.format("%x", din.readShort()));
        System.out.println("Flags: 0x" + String.format("%x", din.readShort()));
        System.out.println("Questions: 0x" + String.format("%x", din.readShort()));
        int answerCount = din.readShort();
        // System.out.println("Answers RRs: 0x" + String.format("%x", din.readShort()));
        System.out.println("Answers RRs: 0x" + String.format("%x", answerCount));
        System.out.println("Authority RRs: 0x" + String.format("%x", din.readShort()));
        System.out.println("Additional RRs: 0x" + String.format("%x", din.readShort()));

        //read the domain name sent using the Standard DNS Name Notation.
        int recLength = 0;
        while ((recLength = din.readByte()) > 0) {
        	byte record[] = new byte[recLength];

        	for (int i = 0; i < recLength; i++) {
        		record[i] = din.readByte();
        	}

        	System.out.println("Record : " + new String(record, "UTF-8"));
        }

        System.out.println("Record Type: 0x" + String.format("%x", din.readShort()));
        System.out.println("Class: 0x" + String.format("%x", din.readShort()));

        //check if any answers obtained.
        if (answerCount == 0) {
        	System.err.println("Server can't find domain!");
        	System.exit(0);
        }
        System.out.println("Field: 0x" + String.format("%x", din.readShort()));
        System.out.println("Type: 0x" + String.format("%x", din.readShort()));
        System.out.println("Class: 0x" + String.format("%x", din.readShort()));
        System.out.println("TTL: 0x" + String.format("%x", din.readInt()));

        short addrLen = din.readShort();
        System.out.println("Len: 0x" + String.format("%x", addrLen));


        String address[] = new String[addrLen];
        for (int i = 0; i < addrLen; i++) {
        	address[i] = String.format("%d", din.readByte() & 0xff);
        }

        System.out.println("Address : " + String.join(".", address));
	}
}
