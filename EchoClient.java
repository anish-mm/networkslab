/*one thread to listen to messages from the server.*/
class ListenerThread extends Thread {
	private BufferedReader buf;

	public ListenerThread(BufferedReader buffer) {
		this.buf = buffer;
	}

	public void run() {
		int i;
		try {
			/* keep reading characters from the buffer.
			   Print if not null */
			while (true) {
				if (i = buf.read() != -1) {
					System.out.print((char)i);
				}
			}
		} catch (Exception e) {
			System.out.println("\n Connection closed.");
		}
	}
}
